﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using E = FootascrazyCarSales.Core.Enquiry;

namespace FootascrazyCarSales.Core
{
    /// <summary>
    /// Base class for all entities presented in the Footascrazy Car Sales system
    /// </summary>
    public abstract class FootascrazyCarSalesEntity
    {
        [Key]
        public long Id { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }

        public FootascrazyCarSalesEntity() { }

        public FootascrazyCarSalesEntity(String name, String description)
        {
            Name = name;
            Description = description;
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append(Name)
                         .Append(" ")
                         .Append(Description);

            return stringBuilder.ToString();
        }
    }
}
