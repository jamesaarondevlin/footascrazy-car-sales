﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

using Mk = FootascrazyCarSales.Core.Make;
using Mdl = FootascrazyCarSales.Core.Model;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FootascrazyCarSales.Core.Car
{
    /// <summary>
    /// Represents a car, a vehicle which ranks lower on the cool scale than a motorcycle
    /// </summary>
    [Table("Car")]
    public class Car : FootascrazyCarSalesEntity
    {
        public virtual Mk.Make Make { get; set; }
        public virtual Mdl.Model Model { get; set; }
        public virtual int Year { get; set; }
        public virtual PriceType PriceType { get; set; }
        public virtual decimal EgcPrice { get; set; }
        public virtual decimal DapPrice { get; set; }
        public virtual String Email { get; set; }
        public virtual String ContactName { get; set; }
        public virtual long PhoneNo { get; set; }
        public virtual long DealerAbn { get; set; }
        public virtual String Comments { get; set; }

        public Car() 
            : base() 
        { }


        public Car(String name, String description)
            : base(name, description)
        { }

        public Car(Mk.Make make, Mdl.Model model,
                   int year, PriceType priceType,
                   decimal egcPrice, decimal dapPrice,
                   String email, String contactName,
                   long phoneNo, long dealerAbn,
                   String comments, String name,
                   String description)
            : base(name, description)
        {
            Make = make;
            Model = model;
            Year = year;
            PriceType = priceType;
            EgcPrice = egcPrice;
            DapPrice = dapPrice;
            Email = email;
            ContactName = contactName;
            PhoneNo = phoneNo;
            DealerAbn = dealerAbn;
            Comments = comments;
            Name = name;
            Description = description;
        }                   
    }
}
