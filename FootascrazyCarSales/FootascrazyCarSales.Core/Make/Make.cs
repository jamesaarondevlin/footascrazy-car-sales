﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using M = FootascrazyCarSales.Core.Model;

namespace FootascrazyCarSales.Core.Make
{
    /// <summary>
    /// Represents a particular make of car (Ford, Honda, Toyota, etc.) 
    /// </summary>
    [Table("Make")]
    public class Make : FootascrazyCarSalesEntity
    {
        public IList<M.Model> Models { get; set; }

        public Make() 
            : base() 
        { }

        public Make(String name, String description)
            : base(name, description)
        { }
    }
}
