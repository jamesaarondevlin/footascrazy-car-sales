﻿namespace FootascrazyCarSales.Core
{
    public enum PriceType
    {
        Invalid = 0,
        PriceOnApplication = 1,
        DriveAwayPrice = 2,
        ExcludingGovtCharges = 3
    }
}
