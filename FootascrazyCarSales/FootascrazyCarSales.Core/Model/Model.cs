﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using M = FootascrazyCarSales.Core.Make;

namespace FootascrazyCarSales.Core.Model
{
    /// <summary>
    /// Represents a particular model of car (Corolla, Accord, etc.) 
    /// produced by a given make (Ford, Honda, Toyota, etc.)
    /// </summary>
    [Table("Model")]
    public class Model : FootascrazyCarSalesEntity
    {
        public M.Make Make { get;set; }

        public Model() 
            : base() 
        { }

        public Model(String name, String description, M.Make make) 
            : base(name, description)
        {
            Make = make;
        }
    }
}
