﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootascrazyCarSales.Core.Enquiry
{
    /// <summary>
    /// Represents an enquiry made on a list
    /// Class can be used for enquiries against a variety of listings (cars, motorcycles, boats, anything as long as you don't have to feed it)
    /// </summary>
    [Table("Enquiry")]
    public class Enquiry : FootascrazyCarSalesEntity
    {
        public FootascrazyCarSalesEntity EntityOfEnquiry { get; set; }
        public String EnquirerName { get; set; }
        public String EnquirerEmail { get; set; }

        public Enquiry() 
            : base() 
        { }

        public Enquiry(string enquirerName, string enquirerEmail) 
            : base()
        { }
    }
}
