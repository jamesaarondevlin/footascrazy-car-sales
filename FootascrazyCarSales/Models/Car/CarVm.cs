﻿using FootascrazyCarSales.Application.Dto.Car;
using FootascrazyCarSales.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace FootascrazyCarSales.Models.Car
{
    public class CarVm
    {
        public CarDto Entity { get; set; }

        #region UI Logic
        public bool DisplayEgcPrice()
        {
            return Entity.PriceType == PriceType.ExcludingGovtCharges;
        }

        public bool DisplayDapPrice()
        {
            return Entity.PriceType == PriceType.DriveAwayPrice;
        }

        /// <summary>
        /// Contact details are only displayed during private sales
        /// </summary>
        public bool DisplayPrivateSaleDetails()
        {
            /// TODO J Clarify business requirement around how private sale
            /// is determined in absence of specific flag. For now this will do.
            return (Entity.PriceType == PriceType.PriceOnApplication);
        }

        /// <summary>
        /// The price displayed is determined by the priceType attribute of CarDto
        /// </summary>
        /// <returns>Price and affix which indicates the price type</returns>
        public String DisplayRelevantPrice()
        {
            StringBuilder relevantPrice = new StringBuilder();

            switch (Entity.PriceType)
            {
                case PriceType.DriveAwayPrice:
                    relevantPrice.Append(Entity.DapPrice); 
                    relevantPrice.Append(" - Drive Away Price");
                    break;

                case PriceType.ExcludingGovtCharges:
                    relevantPrice.Append(Entity.EgcPrice);
                    relevantPrice.Append(" - Excluding Govt. Charges");
                    break;
            }

            return relevantPrice.ToString();
        }
        #endregion
    }
}