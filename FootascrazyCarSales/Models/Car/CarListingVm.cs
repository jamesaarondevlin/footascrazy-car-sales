﻿using FootascrazyCarSales.Application.Dto.Car;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FootascrazyCarSales.Models.Car
{
    public class CarListingVm
    {
        public IList<CarDto> Listings { get; set; }
    }
}