﻿using FootascrazyCarSales.Application.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FootascrazyCarSales.Models.Enquiry
{
    public class EnquiryVm
    {
        public EnquiryDto Entity { get; set; }
    }
}