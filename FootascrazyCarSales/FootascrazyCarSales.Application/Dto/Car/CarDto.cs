﻿using FootascrazyCarSales.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootascrazyCarSales.Application.Dto.Car
{
    public class CarDto : DataTransferObject
    {
        public String Make { get; set; }
        public long MakeId { get; set; }

        public String Model { get; set; }
        public long ModelId { get; set; }

        public int Year { get; set; }
        public PriceType PriceType { get; set; }
        public decimal EgcPrice { get; set; }
        public decimal DapPrice { get; set; }
        public String Email { get; set; }
        public String ContactName { get; set; }
        public long PhoneNo { get; set; }
        public long DealerAbn { get; set; }
        public String Comments { get; set; }
    }
}
