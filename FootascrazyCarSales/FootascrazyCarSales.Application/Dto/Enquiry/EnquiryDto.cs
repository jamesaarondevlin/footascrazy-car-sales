﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootascrazyCarSales.Application.Dto
{
    public class EnquiryDto : DataTransferObject
    {
        public long CarId { get; set; }
        public String CarDescription { get; set; }

        [Required]
        [Display(Name = "Your Name")]
        public String EnquirerName { get; set; }

        [Required]
        [Display(Name = "Your Email")]
        public String EnquirerEmail { get; set; }
    }
}
