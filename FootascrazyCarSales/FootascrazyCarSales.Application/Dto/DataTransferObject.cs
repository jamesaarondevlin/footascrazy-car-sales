﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootascrazyCarSales.Application.Dto
{
    /// <summary>
    /// Base class for all data transfer objects (DTOs) in the
    /// Footascrazy Car Sales system
    /// 
    /// Used to separate display and transmission of information
    /// from back-end encapsulation of data and logic
    /// </summary>
    public abstract class DataTransferObject
    {
        public long Id { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
    }
}
