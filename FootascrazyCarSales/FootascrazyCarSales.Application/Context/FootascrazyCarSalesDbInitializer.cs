﻿using FootascrazyCarSales.Core;
using FootascrazyCarSales.Core.Car;
using FootascrazyCarSales.Core.Make;
using FootascrazyCarSales.Core.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootascrazyCarSales.Application.Context
{
    /// <summary>
    /// Responsible for initializing data
    /// </summary>
    public class FootascrazyCarSalesDbInitializer : DropCreateDatabaseAlways<FootascrazyCarSalesDbContext>
    {
        protected override void Seed(FootascrazyCarSalesDbContext context)
        {
            IList<Make> seedMakes = new List<Make>();
            IList<Model> seedModels = new List<Model>();
            IList<Car> seedCars = new List<Car>();

            Make ford = new Make("Ford", "The Ford Motor Company is an American multinational automaker headquartered in Dearborn, Michigan, a suburb of Detroit.");
            Make holden = new Make("Holden", "GM Holden Ltd, commonly designated Holden, is an Australian automaker that operates in Australasia and is headquartered in Port Melbourne, Victoria.");

            seedMakes.Add(ford);
            seedMakes.Add(holden);

            Model te50 = new Model("TE50","Sedan, performance vehicle", ford);
            Model commodore = new Model("Commodore","P Plate is included", holden);

            seedModels.Add(te50);
            seedModels.Add(commodore);

            seedCars.Add(new Car(ford, te50, 2012, PriceType.ExcludingGovtCharges, 
                                 25000m, 25700m, "contact@fcs.com.au", "contact",
                                 57221000, 12345678, "It's a TE50", te50.Name, te50.Description));

            seedCars.Add(new Car(holden, commodore, 2012, PriceType.DriveAwayPrice, 
                                 26000m, 26700m, "contact@fcs.com.au", "contact",
                                 57221000, 12345678, "It's a TE50", commodore.Name, commodore.Description));

            
            foreach (Make seedMake in seedMakes)
                context.Makes.Add(seedMake);

            foreach (Model seedModel in seedModels)
                context.Models.Add(seedModel);

            foreach (Car seedCar in seedCars)
                context.Cars.Add(seedCar);

            base.Seed(context);
        }
    }
}
