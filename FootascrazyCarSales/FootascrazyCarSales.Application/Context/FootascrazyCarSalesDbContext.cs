﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;

using FootascrazyCarSales.Core.Car;
using FootascrazyCarSales.Core.Make;
using FootascrazyCarSales.Core.Model;
using FootascrazyCarSales.Core.Enquiry;

namespace FootascrazyCarSales.Application.Context
{
    public class FootascrazyCarSalesDbContext : DbContext
    {
        public FootascrazyCarSalesDbContext() : 
            base("FootascrazyCarSalesDb")
        {
            Database.SetInitializer<FootascrazyCarSalesDbContext>(new CreateDatabaseIfNotExists<FootascrazyCarSalesDbContext>());
        }

        public DbSet<Make> Makes { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<Enquiry> Enquiries { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Make>().ToTable("Make");
            modelBuilder.Entity<Model>().ToTable("Model");
            modelBuilder.Entity<Car>().ToTable("Car");
            modelBuilder.Entity<Enquiry>().ToTable("Enquiry");
        }
    }
}
