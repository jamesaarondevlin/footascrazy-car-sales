﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootascrazyCarSales.Application
{
    public enum DbException
    {
        Invalid = 0,
        DbRecordNotFound = 1
    }
}
