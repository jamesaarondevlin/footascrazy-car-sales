﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FootascrazyCarSales.Application.Dto.Car;
using C = FootascrazyCarSales.Core.Car;

namespace FootascrazyCarSales.Application.Mapper.Car
{
    public class CarMapper : DtoMapper<C.Car, CarDto>
    {
        public override void SetDomainModelProperties()
        {
            //TODO J Implement with IsUpdating flag to determine fields to update
        }

        public override void SetDtoProperties()
        {
            DataTransferObject.Make = Model.Make.Name;
            DataTransferObject.MakeId = Model.Make.Id;

            DataTransferObject.Model = Model.Model.Name;
            DataTransferObject.ModelId = Model.Model.Id;

            DataTransferObject.Year = Model.Year;
            DataTransferObject.PriceType = Model.PriceType;
            DataTransferObject.EgcPrice = Model.EgcPrice;
            DataTransferObject.DapPrice = Model.DapPrice;
            DataTransferObject.Email = Model.Email;
            DataTransferObject.ContactName = Model.ContactName;
            DataTransferObject.PhoneNo = Model.PhoneNo;
            DataTransferObject.DealerAbn = Model.DealerAbn;
            DataTransferObject.Comments = Model.Comments;
            DataTransferObject.Name = Model.Name;
            DataTransferObject.Description = Model.Description;   
        }
    }
}
