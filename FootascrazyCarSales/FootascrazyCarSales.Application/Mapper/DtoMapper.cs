﻿using FootascrazyCarSales.Application.Context;
using FootascrazyCarSales.Application.Dto;
using FootascrazyCarSales.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace FootascrazyCarSales.Application.Mapper
{
    /// <summary>
    /// Provides a structure around mapping entity data to a DTO and vice versa
    /// Subclasses should implement mapping specific to the DTO and Entity
    /// </summary>
    /// <typeparam name="Entity">Entity which is persisted in the database and encapsulates relevant business logic</typeparam>
    /// <typeparam name="Dto">Data transfer object which encapsulates all data from the entity in a flat manner using primitive types</typeparam>
    public abstract class DtoMapper<Entity, Dto>
        where Entity : FootascrazyCarSalesEntity, new()
        where Dto : DataTransferObject, new()
    {
        public virtual Entity Model { get; private set; }
        public virtual Dto DataTransferObject { get; private set; }
        public bool IsUpdating { get; private set; }

        public void CreateDomainModel(Dto dto)
        {
            IsUpdating = false;
            DataTransferObject = dto; 
            Model = new Entity();
            SetDomainModelProperties();
        }

        /// <summary>
        /// Map the properties of the dto back to the entity
        /// </summary>
        /// <param name="dto"></param>
        public void UpdateDomainModel(Dto dto)
        {
            using (var context = new FootascrazyCarSalesDbContext())
            {
                this.Model = context.Set<Entity>().FirstOrDefault(x => x.Id == dto.Id);
            }

            SetDomainModelProperties();
        }

        /// <summary>
        /// Map the properties of the entity back to the dto
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateDto(Entity entity)
        {
            this.Model = entity;
            this.DataTransferObject = new Dto();
            this.DataTransferObject.Id = entity.Id;
            SetDtoProperties();
        }

        public abstract void SetDomainModelProperties();
        public abstract void SetDtoProperties();
    }
}
