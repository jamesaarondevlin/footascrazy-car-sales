﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FootascrazyCarSales.Application.Dto;
using E = FootascrazyCarSales.Core.Enquiry;
using FootascrazyCarSales.Application.Context;

namespace FootascrazyCarSales.Application.Mapper.Enquiry
{
    public class EnquiryMapper : DtoMapper<E.Enquiry, EnquiryDto>
    {
        public override void SetDomainModelProperties()
        {
            Model.EnquirerName = DataTransferObject.EnquirerName;
            Model.EnquirerEmail = DataTransferObject.EnquirerEmail;
        }

        public override void SetDtoProperties()
        {
            DataTransferObject.EnquirerName = Model.EnquirerName;
            DataTransferObject.EnquirerEmail = Model.EnquirerEmail;
            
            using(var context = new FootascrazyCarSalesDbContext())
            {
                DataTransferObject.CarDescription = context.Cars.FirstOrDefault(x => x.Id == DataTransferObject.CarId).ToString();
            }
        }
    }
}
