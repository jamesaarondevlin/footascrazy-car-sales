﻿using FootascrazyCarSales.Application.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootascrazyCarSales.Application.Service.Enquiry
{
    /// <summary>
    /// Interface to specify service functions
    /// required to make enquiries against
    /// listings stored in the Footascrazy Car Sales website
    /// </summary>
    public interface IEnquiryService
    {
        void Enquire(EnquiryDto dto);
    }
}
