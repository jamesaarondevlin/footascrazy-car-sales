﻿using FootascrazyCarSales.Application.Context;
using FootascrazyCarSales.Application.Dto;
using FootascrazyCarSales.Application.Logging;
using FootascrazyCarSales.Application.Mapper.Enquiry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootascrazyCarSales.Application.Service.Enquiry
{
    public class EnquiryService : IEnquiryService
    {
        public void Enquire(EnquiryDto dto)
        {
            EnquiryMapper mapper = new EnquiryMapper();

            try
            {
                mapper.CreateDomainModel(dto);

                using (var context = new FootascrazyCarSalesDbContext())
                {
                    context.Enquiries.Add(mapper.Model);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                FootascrazyCarSalesLogger logger = new FootascrazyCarSalesLogger();
                StringBuilder sb = new StringBuilder().Append(e.ToString())
                                                      .Append(" for ")
                                                      .Append(mapper.Model.ToString());
                logger.Error(sb.ToString());
                throw new Exception(e.ToString());
            }
        }
    }
}
