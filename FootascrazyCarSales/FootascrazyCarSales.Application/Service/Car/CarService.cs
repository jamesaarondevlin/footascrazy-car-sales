﻿using FootascrazyCarSales.Application.Context;
using FootascrazyCarSales.Application.Dto.Car;
using FootascrazyCarSales.Application.Logging;
using FootascrazyCarSales.Application.Mapper.Car;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C = FootascrazyCarSales.Core.Car;

namespace FootascrazyCarSales.Application.Service.Car
{
    public class CarService : ICarService
    {
        public void Create(CarDto dto)
        {
            CarMapper mapper = new CarMapper();

            try
            {
                mapper.UpdateDomainModel(dto);

                using (var context = new FootascrazyCarSalesDbContext())
                {
                    context.Cars.Add(mapper.Model);
                }
            }
            catch (Exception e)
            {
                FootascrazyCarSalesLogger logger = new FootascrazyCarSalesLogger();                    
                StringBuilder sb = new StringBuilder().Append(e.ToString())                                                      
                                                      .Append(" for ")
                                                      .Append(mapper.Model.ToString());
                logger.Error(sb.ToString());
                throw new Exception(e.ToString());
            }
        }

        public IList<CarDto> List()
        {
            CarMapper mapper = new CarMapper();
            IList<CarDto> cars = new List<CarDto>();

            using(var context = new FootascrazyCarSalesDbContext())
            {
                foreach(C.Car car in context.Cars)
                {
                    mapper.UpdateDto(car);
                    cars.Add(mapper.DataTransferObject);
                }
            }

            return cars;
        }

        public CarDto FindById(long id)
        {
            CarMapper mapper = new CarMapper();

            using (var context = new FootascrazyCarSalesDbContext())
            {
                C.Car car = context.Cars.FirstOrDefault(x => x.Id == id);

                if (car != null)
                    mapper.UpdateDto(car);
                else
                {
                    FootascrazyCarSalesLogger logger = new FootascrazyCarSalesLogger();
                    StringBuilder sb = new StringBuilder().Append(DbException.DbRecordNotFound.ToString())
                                                          .Append(" for ")
                                                          .Append(car.ToString());
                    logger.Error(sb.ToString());
                    throw new Exception(DbException.DbRecordNotFound.ToString());
                }
            }

            return mapper.DataTransferObject;
        }

        public void Update(CarDto carDto)
        {
            CarMapper mapper = new CarMapper();

            try
            {
                using (var context = new FootascrazyCarSalesDbContext())
                {
                    mapper.UpdateDomainModel(carDto);

                    context.Entry(mapper.Model).State = EntityState.Modified;
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                FootascrazyCarSalesLogger logger = new FootascrazyCarSalesLogger();
                StringBuilder sb = new StringBuilder().Append(e.ToString())
                                                      .Append(" for ")
                                                      .Append(mapper.Model.ToString());
                logger.Error(sb.ToString());
                throw new Exception(DbException.DbRecordNotFound.ToString());
            }
        }

        public void Delete(long id)
        {
            try
            {
                using (var context = new FootascrazyCarSalesDbContext())
                {
                    C.Car car = context.Cars.FirstOrDefault(x => x.Id == id);
                    context.Cars.Remove(car);
                }
            }
            catch (Exception e)
            {
                FootascrazyCarSalesLogger logger = new FootascrazyCarSalesLogger();
                logger.Error(e.ToString());
                throw new Exception(e.ToString());
            }
        }
    }
}
