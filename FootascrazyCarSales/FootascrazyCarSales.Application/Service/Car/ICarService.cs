﻿using FootascrazyCarSales.Application.Dto.Car;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C = FootascrazyCarSales.Core.Car;

namespace FootascrazyCarSales.Application.Service.Car
{
    /// <summary>
    /// Interface to specify service functions
    /// required for interactions with Car related data
    /// </summary>
    public interface ICarService
    {
        #region Create
        void Create(CarDto car);
        #endregion

        #region Read
        IList<CarDto> List();
        CarDto FindById(long id);
        #endregion

        #region Update
        void Update(CarDto car);
        #endregion

        #region Delete
        void Delete(long id);
        #endregion
    }
}
