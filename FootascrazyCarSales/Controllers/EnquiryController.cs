﻿using FootascrazyCarSales.Application.Dto;
using FootascrazyCarSales.Application.Service.Enquiry;
using FootascrazyCarSales.Models.Enquiry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FootascrazyCarSales.Controllers
{
    public class EnquiryController : Controller
    {
        //
        // GET: /Enquiry/

        public ActionResult Create(long id)
        {
            EnquiryVm model = new EnquiryVm() { Entity = new EnquiryDto() { CarId = id } };
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(EnquiryVm model)
        {
            if (!ModelState.IsValid)
                return View();

            EnquiryService service = new EnquiryService();
            service.Enquire(model.Entity);

            return View("EnquirySubmitted", model);
        }

    }
}
