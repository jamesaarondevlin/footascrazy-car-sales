﻿using FootascrazyCarSales.Application.Service.Car;
using FootascrazyCarSales.Models.Car;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FootascrazyCarSales.Controllers
{
    public class CarController : Controller
    {
        //
        // GET: /Car/

        public ActionResult List()
        {
            CarService service = new CarService();
            CarListingVm carListingVm = new CarListingVm();
            carListingVm.Listings = service.List();
            return View(carListingVm);
        }

        public ActionResult Detail(long id)
        {
            CarService service = new CarService(); 
            CarVm carVm = new CarVm();
            carVm.Entity = service.FindById(id);

            return View(carVm);
        }

    }
}
